use std::env;
use std::error::Error;
use std::fs;

#[derive(Copy, Clone)]
pub struct Config<'a> {
    query: &'a String,
    filenames: &'a [String],
    case_sensitive: bool,
}

impl<'a> Config<'a> {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        // Error handling
        if args.len() < 3 {
            Err("not enough arguments")
        } else {
            let query = &args[1];
            let filenames = &args[2..];
            // Checks to see if the var is set; if it is then great.
            let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

            Ok(Config {
                query,
                filenames,
                case_sensitive,
            })
        }
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    for filename in config.filenames {
        let contents = fs::read_to_string(filename)?;
        let results = if config.case_sensitive {
            search(&config.query, &contents)
        } else {
            search_case_insensitive(&config.query, &contents)
        };

        if results.is_empty() {
            println!("=== {} ===", filename.to_uppercase());
            for (i, line) in &results {
                println!("{}: {}", i, line);
            }
        }
    }

    Ok(())
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<(usize, &'a str)> {
    contents
        .lines()
        .enumerate()
        .filter(|(_, line)| line.contains(&query))
        .collect()
}

pub fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<(usize, &'a str)> {
    let query = query.to_lowercase();

    contents
        .lines()
        .enumerate()
        .filter(|(_, line)| line.to_lowercase().contains(&query))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn one_result() {
        let query = "duct";
        let contents = "Rust:\nsafe, fast, productive.\nPick three.";

        assert_eq!(
            vec![(1, "safe, fast, productive.")],
            search(query, contents)
        );
    }

    #[test]
    fn two_results() {
        let query = " ";
        let contents = "Rust:\nsafe, fast, productive.\nPick three.";

        assert_eq!(
            vec![(1, "safe, fast, productive."), (2, "Pick three.")],
            search(query, contents)
        );
    }

    #[test]
    fn case_insensitive() {
        let query = "rUST";
        let contents = "Rust:\nsafe, fast, productive.\nPick three.\nTrust me.";

        assert_eq!(
            vec![(0, "Rust:"), (3, "Trust me.")],
            search_case_insensitive(query, contents)
        );
    }
}
